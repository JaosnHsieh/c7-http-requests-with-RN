import React from "react";
import { View, Text } from "react-native";
import { Header } from "react-native-elements";
import axios from "axios";
import AlbumDetails from "./AlbumDetails";
export default class AlbumList extends React.Component {
  state = {
    albums: []
  };
  componentWillMount() {
    console.log("album list");
    axios
      .get("http://rallycoding.herokuapp.com/api/music_albums")
      .then(responses => {
        this.setState({
          albums: responses.data || []
        });
        console.log(this.state);
      })
      .catch(err => console.log("err", err));
  }
  renderAlbum() {
    return this.state.albums.map(a => <AlbumDetails key={a.title} album={a} />);
  }
  render() {
    return (
      <View>
        <Text> album list </Text>
        {this.renderAlbum()}
      </View>
    );
  }
}
