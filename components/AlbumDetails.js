import React from "react";
import { View, Text } from "react-native";
import Card from "./Card";
import CardSection from "./CardSection";

const AlbumDetails = props => {
  const { album } = props;
  return (
    <Card>
      <CardSection>
        <Text key={album.title}>{album.title}</Text>
      </CardSection>
    </Card>
  );
};

export default AlbumDetails;
