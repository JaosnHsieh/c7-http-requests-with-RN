import React from "react";
import { View, Text } from "react-native";

const Card = props => {
  return (
    <View
      style={{
        borderWidth: 2,
        borderRadius: 2,
        borderColor: "#ddd",
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10
      }}
    >
      {props.children}
    </View>
  );
};

export default Card;
